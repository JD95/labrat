{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeInType #-}
{-# LANGUAGE TypeApplications #-}

module Lib
    ( someFunc
    ) where

import GLSL

type Bar =
  'S "bar"
  '[ 'Val 'Bool "test"]

type Foo =
  'S "foo"
  '[ 'Val 'Int "test"
   , 'Val 'Double "bar"
   , 'Val Bar "foog"
   ]

type AddOne =
  'Function "add_one" '[ 'Val 'Int "x" ] 'Int
  '[ "x" ':= 5
   , 'Return "x"
   ]

type Definitions =
  '[ 'Def Foo
   , 'Def Bar
   ]

type Shader 
  = 'Function "main" '[] 'Void '[]

test = build (Proxy @Definitions) (Proxy @Shader)

someFunc :: IO ()
someFunc = putStrLn "someFunc"
