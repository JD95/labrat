{-# LANGUAGE ConstraintKinds            #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TypeFamilyDependencies     #-}
{-# LANGUAGE UndecidableInstances       #-}
{-# LANGUAGE TypeOperators              #-}
{-# LANGUAGE GADTs                      #-}

module GLSL.Language where

import           Control.Arrow
import           Control.Monad              hiding (return)
import           Control.Monad.State.Strict hiding (return)
import           Data.Proxy
import           Data.Semigroup
import           Prelude                    hiding (mod, not, return)
import GHC.TypeLits

class Expr r where
  int :: Int -> r Int
  mod :: r Int -> r Int -> r Int
  eq :: r a -> r a -> r Bool
  ifelse :: r Bool -> r () -> Maybe (r ()) -> r ()
  not :: r Bool -> r Bool
  var :: r a -> r (r a)
  (=:) :: r a -> r a -> r ()
  while :: r Bool -> r () -> r ()
  return :: r a -> r a
  args :: Args xs -> r ()
  call :: (CompileArgs (Args xs), KnownSymbol name) => proxy r -> Func name a -> Args xs -> r (Output r a)

newtype Func (name :: Symbol) a = Func a

data Args (xs :: [*]) where
  ANil :: Args '[]
  (:.) :: x -> Args xs -> Args (x ': xs)

if_ :: (Expr r) => r Bool -> r () -> r ()
if_ cond body = ifelse cond body Nothing 

class CallFunc r a b where
  callFunc :: proxy r -> a -> b -> Output r a

instance (b ~ Output r (r a -> b)) => CallFunc r (r a -> b) (Args '[ r a ]) where
  callFunc r f (x :. ANil) = f x

instance (CallFunc r (r b -> c) (Args xs)) 
  => CallFunc r (r a -> (r b -> c)) (Args (r a ': xs)) where
  callFunc r f (x :. xs) = callFunc r (f x) xs

for :: (Monad r, Expr r)
  => r (r a)
  -> (r a -> r Bool)
  -> (r a -> r ())
  -> (r a -> r ())
  -> r ()
for counter cond update body = do
  i <- counter
  while (cond i) (body i *> update i)

type family IsFunc f :: Bool where
  IsFunc (a -> b) = 'True
  IsFunc f = 'False

class Var r a where
  decl :: proxy a -> r (r a)
  typ :: proxy a -> r ()

type family Output (r :: * -> *) f where
  Output r (a -> b) = Output r b
  Output r (r b) = b

class (Functor r) => Inputs (isFunc :: Bool) r a where
  inputs :: p isFunc -> a -> r ([r ()], r (Output r a))

instance (Monad r, Var r a, Inputs (IsFunc b) r b)
  => Inputs 'True r (r a -> b) where
  inputs _ f = do
    x <- decl (Proxy @a)
    (xs, f') <- inputs (Proxy @(IsFunc b)) (f x)
    pure $ (fmap (const ()) x : xs, f')

instance (b ~ Output r (r b), Monad r, Var r b)
  => Inputs 'False r (r b) where
  inputs _ x = pure ([], x)
