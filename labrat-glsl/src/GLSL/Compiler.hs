{-# LANGUAGE ConstraintKinds            #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeOperators              #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TypeFamilyDependencies     #-}
{-# LANGUAGE UndecidableInstances       #-}

module GLSL.Compiler where

import           Control.Arrow
import           Control.Monad              hiding (return)
import           Control.Monad.State.Strict hiding (return)
import           Data.List
import           Data.Proxy
import           Data.Semigroup
import           GHC.TypeLits
import           Prelude                    hiding (mod, not, return)

import           GLSL.Language

newtype Compile a
  = Compile
  { compile :: State (Int, String) a
  } deriving newtype (Functor, Applicative, Monad)

write :: (MonadState (a,s) m, Semigroup s) => s -> m ()
write x = modify (second (<> x))

instance Var Compile Int where
  decl _ = Compile $ do
    (n, _) <- get
    let name = "x" <> show n
    (write $ "int " <> name)
    modify (first (+1))
    pure (Compile $ write name *> pure undefined)
  typ _ = Compile (write "int")

define ::
  ( Var Compile (Output Compile b)
  , Inputs 'True Compile (Compile a -> b)
  ) => String
  -> (Compile a -> b)
  -> Compile ()
define name (body :: (Compile a -> b)) = do
  typ (Proxy @ (Output Compile b))
  Compile (write $ " " <> name <> " (")
  (args, f) <- inputs (Proxy @'True) body
  foldr (*>) (pure ())
    . intersperse (Compile $ write ", ")
    $ args
  Compile (write "){")
  _ <- f
  Compile (write "}")

instance Expr Compile where
  int n = Compile $ do
    write (show n)
    pure (error "Int used!")

  mod a b = Compile $ do
    _ <- compile a
    write " % "
    compile b

  eq a b = Compile $ do
    _ <- compile a
    write " == "
    _ <- compile b
    pure (error "Eq used!")

  not b = Compile $ do
    write "!"
    compile b

  var x = Compile $ do
    n <- fst <$> get
    modify (first (+1))
    let name = "x" <> show n
    write (name <> " = ")
    _ <- compile x
    write ";"
    pure (Compile $ write name *> pure undefined)

  while b body = Compile $ do
    write "while ("
    _ <- compile b
    write ") {"
    compile body
    write ("}")

  (=:) lval rval = Compile $ do
    _ <- compile lval
    write " = "
    _ <- compile rval
    write ";"

  return value = Compile $ do
    write "return "
    _ <- compile value
    write ";"
    pure undefined
  
  ifelse = undefined

  call _ (Func f :: Func name _) xs = Compile $ do
    write $ (symbolVal (Proxy @name)) <> "("
    compileArgs xs
    write $ ")"
    pure undefined

class CompileArgs a where
  compileArgs :: a -> State (Int, String) () 

instance (CompileArgs (Args xs)) => CompileArgs (Args ((Compile a) ': xs)) where
  compileArgs (x :. xs) = compile x *> write ", " *> compileArgs xs

instance CompileArgs (Args '[ Compile a ]) where
  compileArgs (x :. ANil) = compile x *> pure ()

runCompiler :: Compile a -> String
runCompiler (Compile s) = snd $ execState s (0, "")
