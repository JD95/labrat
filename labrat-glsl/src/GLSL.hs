module GLSL
  ( module GLSL.Language
  , module GLSL.Compiler
  ) where

import           Prelude       hiding (mod, not, return)

import           GLSL.Compiler
import           GLSL.Language

test :: (Monad r, Expr r) => r Int -> r Int
test a = do
  for (var (int 0)) (eq (int 5)) (\i -> i =: int 4) $ \i -> do
    i =: (i `mod` (int 5))
  return a

foo :: String
foo = runCompiler (define "test" test)
